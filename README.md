# scripts

Where I store all relevant scripts I used to make.

## Organization
The scripts are made considering:
- `~/.local/bin` is into PATH variable.
- Any relevant data is stored on `~/.local/share/scripts/$script_name/` directory.
- Every script is a single executable file tha may or not contain auxiliar
components on `~/.local/lib/scripts/$script_name/` directory.
- The script directory is structured in the follow way:
    - `script_name/*.sh`: the main executables. They will be installed without
    .sh suffix on `~/.local/bin`.
    - `script_name/share`: things to be installed on
    `~/.local/share/scripts/$script_name/`
    - `script_name/lib` things to be installed on
    `~/.local/lib/scripts/$script_name/`

## Management
I prepared the `script_manager.sh` script to manage the other scripts
installation, see it help message.

The install consists of make symbolic links to each script into
respective directories.
